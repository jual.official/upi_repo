import 'package:flutter/widgets.dart';
import 'package:flutter/material.dart';
import 'package:helloworld/pages/architect.dart';
import 'package:helloworld/pages/civil.dart';
import 'package:helloworld/pages/cse.dart';
import 'package:helloworld/pages/eee.dart';

void main() => runApp(depertment());

class depertment extends StatelessWidget {
  const depertment({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          actions: [Icon(Icons.roundabout_left)],
          title: Text(
            "UPI Depertment",
            style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
          ),
        ),
        body: SingleChildScrollView(
          child: Column(children: [
            //cse
            Padding(
              padding: const EdgeInsets.all(5.0),
              child: InkWell(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => const cse()),
                  );
                },
                child: Container(
                  decoration: BoxDecoration(
                      color: Colors.lightBlueAccent,
                      borderRadius: BorderRadius.circular(20)),
                  child: Center(
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(5.0),
                          child: Container(
                            height: 100,
                            width: 100,
                            child: Image(
                              image: AssetImage("assets/images/cse.png"),
                              fit: BoxFit.contain,
                            ),
                          ),
                        ),
                        Text(
                          "CSE",
                          style: TextStyle(
                              fontSize: 30, fontWeight: FontWeight.w500),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
            //EEE
            Padding(
              padding: const EdgeInsets.all(5.0),
              child: InkWell(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => const eee()),
                  );
                },
                child: Container(
                  decoration: BoxDecoration(
                      color: Colors.lightBlueAccent,
                      borderRadius: BorderRadius.circular(20)),
                  child: Center(
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(5.0),
                          child: Container(
                            height: 100,
                            width: 100,
                            child: Image(
                              image: AssetImage("assets/images/electric.png"),
                              fit: BoxFit.contain,
                            ),
                          ),
                        ),
                        Text(
                          "EEE",
                          style: TextStyle(
                              fontSize: 30, fontWeight: FontWeight.w500),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
            //civil
            Padding(
              padding: const EdgeInsets.all(5.0),
              child: InkWell(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => const civil()),
                  );
                },
                child: Container(
                  decoration: BoxDecoration(
                      color: Colors.lightBlueAccent,
                      borderRadius: BorderRadius.circular(20)),
                  child: Center(
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(5.0),
                          child: Container(
                            height: 100,
                            width: 100,
                            child: Image(
                              image: AssetImage("assets/images/civil.png"),
                              fit: BoxFit.contain,
                            ),
                          ),
                        ),
                        Text(
                          "Civil",
                          style: TextStyle(
                              fontSize: 30, fontWeight: FontWeight.w500),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
            //architect
            Padding(
              padding: const EdgeInsets.all(5.0),
              child: InkWell(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => const architech()),
                  );
                },
                child: Container(
                  decoration: BoxDecoration(
                      color: Colors.lightBlueAccent,
                      borderRadius: BorderRadius.circular(20)),
                  child: Center(
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(5.0),
                          child: Container(
                            height: 100,
                            width: 100,
                            child: Image(
                              image: AssetImage("assets/images/architect.png"),
                              fit: BoxFit.contain,
                            ),
                          ),
                        ),
                        Text(
                          "Architecture",
                          style: TextStyle(
                              fontSize: 26, fontWeight: FontWeight.w500),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ]),
        ),
      ),
    );
  }
}
