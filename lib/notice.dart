import 'package:flutter/widgets.dart';
import 'package:flutter/material.dart';

void main() => runApp(Notice());

class Notice extends StatefulWidget {
  const Notice({Key? key}) : super(key: key);
  _NoticeState createState() => _NoticeState();
}

class _NoticeState extends State<Notice> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          actions: [Icon(Icons.roundabout_left)],
          title: Text(
            "UPI Notice",
            style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
          ),
        ),
        body: Column(children: []),
      ),
    );
  }
}
