import 'dart:html';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:helloworld/homepage.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      // Application name
      title: 'Flutter Hello World',
      // Application theme data, you can set the colors for the application as
      // you want
      theme: ThemeData(primarySwatch: Colors.lightBlue),
      // A widget which will be started on application startup
      home: MyHomePage(title: 'Uttara Polytechnic Institut'),
    );
  }
}

class MyHomePage extends StatelessWidget {
  final String title;
  const MyHomePage({super.key, required this.title});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          drawer: Drawer(
              backgroundColor: Color(0x62e5c8c8),
              child: Column(children: [
                DrawerHeader(
                    child: Icon(
                  Icons.facebook,
                  size: 50,
                )),
                Container(
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(50)),
                  height: 100,
                  width: 100,
                  child: Image(
                    image: AssetImage("assets/images/me.jpg"),
                  ),
                ),
                Text(
                  "Md Ziaur Rahman Jual",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 25,
                      fontWeight: FontWeight.bold),
                ),
                SizedBox(height: 10),
                Text(
                  "Uttara Polytechnic Institute",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                      fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  "Email: Jualc18@gmail.com",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 12,
                      fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  "www.Facebook.com/ziaur.jual",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 12,
                      fontWeight: FontWeight.bold),
                ),
              ])),
          appBar: AppBar(
              title: Text(
            "Uttara Polytechnic Institute",
            style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
          )),
          body: homepage()),
    );
  }
}
